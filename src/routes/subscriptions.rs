use actix_web::{HttpResponse, web::Form};

#[derive(serde::Deserialize)]
pub struct FormData {
    email: String,
    name: String
}

// Always return 200 OK
pub async fn subscribe(_form: Form<FormData>) -> HttpResponse {
    HttpResponse::Ok().finish()
}